To run the program first you have to compile using Cmake.
To do that, make a build directory in the project
```
mkdir build
cd ./build
cmake ..
cmake --build .
```

and run the program using 
```
./project <args>
```
