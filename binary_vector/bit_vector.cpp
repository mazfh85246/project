//
// Created by matthew on 19.05.22.
//


#include <stdexcept>
#include "bit_vector.h"

void Leaf::flip(int64_t pos) {
    // locate correct position and flip that bit
    int64_t index = pos >> 4;
    int16_t bit_index = 15 - (pos & 0b1111);
    int16_t x = 0b1;

    // adjusts num ones, according to if we add or remove a one
    if (data[index] & (x << bit_index)) {
        num_ones--;
    } else {
        num_ones++;
    }

    this->data[index] ^= (x << bit_index);
}

int64_t Leaf::rank_0(int64_t pos) {
    int64_t rank = 0;
    int64_t index = pos >> 4;
    int64_t number_of_bits = pos & 0b1111;
    int64_t bit_index = 16 - number_of_bits;
    // loop through the entries in this vector, and count the number of zeros, until the desired offset is reached
    for (int i = 0; i < index; ++i) {
        int64_t temp = ~this->data[i];
        for (int j = 0; j < 16; ++j) {
            rank += temp & 0x1;
            temp >>= 1;
        }
    }
    // We do not want to iterate through the entirety of the last int. So we have a separate for loop through the
    // desired amount of the last int
    int64_t temp = ~this->data[index] >> (bit_index);
    for (int i = 0; i < number_of_bits; ++i) {
        rank += temp & 0x1;
        temp >>= 1;
    }
    return rank;
}

int64_t Leaf::rank_1(int64_t pos) {
    int64_t rank = 0;
    int64_t index = pos >> 4;
    int64_t number_of_bits = pos & 0b1111;
    int64_t bit_index = 16 - number_of_bits;
    // loop through the entries in this vector, and count the number of ones, until the desired position is reached
    for (int i = 0; i < index; ++i) {
        int64_t temp = this->data[i];
        for (int j = 0; j < 16; ++j) {
            rank += temp & 0x1;
            temp >>= 1;
        }
    }

    // We do not want to iterate through the entirety of the last int. So we have a separate for loop through the
    // desired amount of the last int
    int64_t temp = this->data[index] >> (bit_index);
    for (int i = 0; i < number_of_bits; ++i) {
        rank += temp & 0x1;
        temp >>= 1;
    }
    return rank;
}

int64_t Leaf::select_1(int64_t pos) {
    int64_t count = 0;
    // count the number of ones, until the desired number of ones is reached.
    for (int64_t temp: this->data) {
        for (int j = 15; j >= 0; --j) {
            pos -= temp >> j & 0b1;
            if (pos == 0) {
                return count;
            }
            count++;
        }
    }
    // If there are not enough ones to reach the desired amount, then something has gone terribly wrong
    throw std::invalid_argument("ith 1 index not found");
}

int64_t Leaf::select_0(int64_t pos) {
    int64_t count = 0;
    // count the number of zeros, until the desired number of ones is reached.
    for (int64_t temp: this->data) {
        temp = ~temp;
        for (int j = 15; j >= 0; j--) {
            pos -= (temp >> j) & 0b1;
            if (pos == 0) {
                return count;
            }
            count++;
        }
    }
    // If there are not enough zeros to reach the desired amount, then something has gone terribly wrong
    throw std::invalid_argument("ith 1 index not found");
}

Leaf::Leaf(const std::vector<uint16_t> &data, int64_t begin, int64_t end) {
    int64_t si = end - begin;
    for (int64_t j = 0; j < si; ++j) {
        this->data[j] = data[j + begin];
    }
    this->used_up = end - begin;
    this->size = used_up * 16;

    update_num_ones();
}

void Leaf::remove(int64_t pos) {
    // find out by how many bits to the left we need to shift to get the relevant bit in the MSF position
    uint16_t bit_index = pos & 0b1111;
    uint64_t index = pos >> 4;

    // gets the value of the bit to be removed, so I can adjust parameters
    if ((data[index] >> (15 - bit_index)) & 0b1) {
        num_ones--;
    }

    uint16_t remove_lower = (16 - bit_index);
    data[index] = ((data[index] >> remove_lower) << (remove_lower - 1)) |
                  ((((data[index] << (bit_index + 1)) & 0xffff) >> bit_index) >> 1);

    for (uint64_t i = index; i < used_up; ++i) {
        // don't worry, our array is big enough so that this does not cause memory leaks!
        data[i] = (data[i] << 1) | (data[i + 1] >> 15);
    }
    size--;
    if ((size & 0b1111) == 0) {
        used_up--;
    }
}

void Leaf::merge_right(Vertexy *pVertexy) {
    if (!pVertexy->is_Leaf()) {
        // If the given vertex is not a leaf, then we have to go deeper in the tree
        merge_right(((Vertex *) pVertexy)->left);
        // we don't know what type of wacky stuff has happened below us. se we need to update, and check if we have
        // deleted any of our children
        ((Vertex *) pVertexy)->update();
        ((Vertex *) pVertexy)->checkLeftKid();
        return;
    }
    auto *leaf = ((Leaf *) pVertexy);
    // the shift required to put bits at the end of the int;
    int64_t right_shift = this->size & 0b1111;
    right_shift = (right_shift == 0) ? 16 : right_shift;
    // the shift required to put bits at the beginning of the int
    int64_t left_shift = 16 - right_shift;
    if (this->used_up + leaf->used_up <= MAX_SIZE || parent->parent == nullptr) {
        // We can now simply merge the two, into one
        const int64_t till = leaf->used_up;
        for (int64_t i = 0; i < till; ++i) {
            this->data[used_up - 1 + i] |= leaf->data[i] >> right_shift;
            this->data[used_up + i] |= leaf->data[i] << left_shift;
        }
        this->size += leaf->size;
        this->used_up = this->size >> 4;
        this->used_up += (this->size % 16 == 0) ? 0 : 1;
        this->num_ones += leaf->num_ones;
        // We then invalidate the leaf, to be deleted by the parent
        leaf->size = 0;
        leaf->height = 0;
        leaf->used_up = 0;
        leaf->num_ones = 0;
    } else {
        // In this case, we transfer data from the smaller one to the larger one
        if (used_up > leaf->used_up) {
            const int64_t to_transfer = (used_up - leaf->used_up) / 2;
            // in this case, we need to transfer data from the left vertex, to the right vertex
            // first, move everything in the recipient over to the right
            for (int64_t i = leaf->used_up - 1; i != -1; --i) {
                // the first is an or, in order to maintain data placed by previous steps
                leaf->data[i + to_transfer] |= leaf->data[i] << left_shift;
                //the second is just an equal to remove the data in data[i+tt-1] before writing
                leaf->data[i + to_transfer - 1] = leaf->data[i] >> right_shift;
            }
            // now we have created space for the data to be moved into
            for (int64_t i = 0; i < to_transfer - 1; ++i) {
                leaf->data[i] = data[used_up - to_transfer + i];
                data[used_up - to_transfer + i] = 0;
            }
            leaf->data[to_transfer - 1] |= data[used_up - 1];
            data[used_up-1] = 0;
            // we have aligned the left leaf to 64 bits.
            int64_t old_size = size;
            size = (size % 16 == 0) ? ((size >> 4) - to_transfer) << 4 : ((size >> 4) + 1 - to_transfer) << 4;
            used_up = size >> 4;
            used_up += (size % 16 == 0) ? 0 : 1;
            leaf->size += old_size - size;
            leaf->used_up = leaf->size >> 4;
            leaf->used_up += leaf->size % 16 == 0 ? 0 : 1;
        } else {
            const int64_t to_transfer = (leaf->used_up - used_up) / 2;
            // in this case, we need to transfer data from the right to the left
            for (int i = 0; i < to_transfer; ++i) {
                data[used_up - 1 + i] |= leaf->data[i] >> right_shift;
                data[used_up + i] = leaf->data[i] << left_shift;
            }
            for (int64_t i = to_transfer; i < leaf->used_up; ++i) {
                leaf->data[i - to_transfer] = leaf->data[i];
                leaf->data[i] = 0;
            }
            leaf->used_up -= to_transfer;
            used_up += to_transfer;
            size += to_transfer * 16;
            leaf->size -= to_transfer * 16;
        }
        int64_t old_num_ones = num_ones;
        update_num_ones();
        leaf->num_ones += old_num_ones - num_ones;
    }
}

void Leaf::add(int64_t to_add, int64_t pos) {
    // here we can assume that we have enough space to add it, since splitting is handled by the parent.
    int64_t offset = pos >> 4;
    int64_t bitset = (pos & 0b1111);

    for (int64_t i = used_up; i > offset; --i) {
        data[i] >>= 1;
        data[i] |= data[i - 1] << 15;
    }
    // put the desired bit in the right position
    data[offset] = (data[offset] >> (16 - bitset) << (16 - bitset)) | ((to_add << (15 - bitset))) |
                   (((data[offset] << bitset) & 0xffff) >> (bitset + 1));

    if ((size & 0b1111) == 0) {
        used_up++;
    }
    size++;
    if (to_add == 1) {
        num_ones++;
    }
}

int64_t Vertex::select_1(int64_t pos) {
    // Check if we have to go to the left or the right subtree
    if (pos <= left->num_ones) {
        return left->select_1(pos);
    }
    return left->size + right->select_1(pos - left->num_ones);
}

int64_t Vertex::select_0(int64_t pos) {
    // Check if we have to go to the left or the right subtree
    int64_t num_zeroes = left->size - left->num_ones;
    if (pos <= num_zeroes) {
        return left->select_0(pos);
    }
    return left->size + right->select_0(pos - num_zeroes);
}

int64_t Vertex::rank_1(int64_t pos) {
    // Check if we have to go to the left or the right subtree
    if (pos < left->size) {
        return left->rank_1(pos);
    }
    return left->num_ones + right->rank_1(pos - left->size);
}

int64_t Vertex::rank_0(int64_t pos) {
    // Check if we have to go to the left or the right subtree
    int64_t num_zeroes = left->size - left->num_ones;
    if (pos < left->size) {
        return left->rank_0(pos);
    }
    return num_zeroes + right->rank_0(pos - left->size);
}

void Vertex::remove(int64_t pos) {
    // Check if we have to go to the left or the right subtree
    if (pos < left->size) {
        left->remove(pos);
    } else {
        right->remove(pos - left->size);
    }
    // If the size of one of our children is too small then we should merge them
    if (left->size <= MIN_SIZE << 4 || right->size <= MIN_SIZE << 4) {
        left->merge_right(right);
        // check if we invalidate our left child, if so, delete this, and move our right vertex up
        checkLeftKid();
        if (right != nullptr) {
            // if our right vertex is not null, then we have not deleted this, and this we should check if right was
            // invalidated
            checkRightKid();
        }
    }
    // if this has not been deleted, then let us update our parameters, and then check for rebalancing
    if (left != nullptr && right != nullptr) {
        update();
        rotate();
    }
}

void Vertex::rotate_right() {
    // Balance tree by rotating to the right if necessary
    auto *l = (Vertex *) left;
    left = l->right;
    left->parent = this;

    l->right = this;
    if (parent != nullptr) {
        if (parent->left == this) {
            parent->left = l;
        } else {
            parent->right = l;
        }
        l->parent = parent;
    } else {
        l->parent = nullptr;
    }
    parent = l;
    update();
    l->update();
}

void Vertex::rotate_left() {
    // Balance tree by rotating to the left if necessary
    auto *r = (Vertex *) right;
    right = r->left;
    right->parent = this;

    r->left = this;
    if (parent != nullptr) {
        if (parent->left == this) {
            parent->left = r;
        } else {
            parent->right = r;
        }
        r->parent = parent;
    } else {
        r->parent = nullptr;
    }
    parent = r;
    update();
    r->update();
}

void Vertex::update() {
    // Update the parameters using the parameters of the children
    height = std::max(left->height, right->height) + 1;
    num_ones = left->num_ones + right->num_ones;
    size = left->size + right->size;
}

void Vertex::merge_right(Vertexy *pVertexy) {
    // If merge_right is called on a vertex, then we need to find the rightmost leaf below us, and then update, in the
    // case we messed some stuff up
    right->merge_right(pVertexy);
    update();
    checkRightKid();
}

void Vertex::checkLeftKid() {
    // check if the left child has been deleted, if so then move the right vertex up and delete this
    if (left->size == 0 && parent != nullptr) {
        if (this->parent->left == this) {
            this->parent->left = right;
        } else {
            this->parent->right = right;
        }
        right->parent = parent;
        right = nullptr;
        delete this;
    } else {
        if (!left->is_Leaf()) {
            ((Vertex *) left)->checkLeftKid();
        }
    }
}

void Vertex::checkRightKid() {
    // check if the right child has been deleted, if so then move the left vertex up and delete this
    if (right->size == 0 && parent != nullptr) {
        if (this->parent->left == this) {
            this->parent->left = left;
        } else {
            this->parent->right = left;
        }
        left->parent = parent;
        left = nullptr;
        delete this;
    } else {
        if (!right->is_Leaf()) {
            ((Vertex *) right)->checkRightKid();
        }
    }
}

void Vertex::add(int64_t to_add, int64_t pos) {
    // First check if we add to the left or the right subtree. Then check if our child is too big. If that is the case,
    // then split the leaf into multiple leaves
    if (pos < left->size) {
        if (left->is_Leaf() && left->size >> 4 == MAX_SIZE) {
            auto v = new Vertex(
                    std::vector<uint16_t>(((Leaf *) left)->data, ((Leaf *) left)->data + MAX_SIZE), 0,
                    MAX_SIZE);
            delete left;
            left = v;
            left->parent = this;
            update();
        }
        left->add(to_add, pos);
    } else {
        if (right->is_Leaf() && right->size >> 4 == MAX_SIZE) {
            auto v = new Vertex(
                    std::vector<uint16_t>(((Leaf *) right)->data, ((Leaf *) right)->data + MAX_SIZE), 0,
                    MAX_SIZE);
            delete right;
            right = v;
            right->parent = this;
            update();
        }
        right->add(to_add, pos - left->size);
    }
    // update and rotate, since we change the subtree
    update();
    rotate();
}

Vertex::Vertex(const std::vector<uint16_t> &data, int64_t begin, int64_t end) {
    const int64_t num_els = end - begin;
    int64_t l_size = (num_els / 2) + 1;
    int64_t fend = begin + l_size;

    if (num_els > MAX_SIZE) {
        left = new Vertex(data, begin, fend);
        left->parent = this;
        right = new Vertex(data, fend, end);
        right->parent = this;
    } else {
        left = new Leaf(data, begin, fend);
        right = new Leaf(data, fend, end);
    }
    left->parent = this;
    right->parent = this;
    update();
}


void Vertex::flip(int64_t pos) {
    // check if we have to go left or right
    if (pos < left->size) {
        left->flip(pos);
    } else {
        right->flip(pos - left->size);
    }
    update();
}

Vertex::~Vertex() {
    // delete ourselves and both of our kids.
    delete left;
    delete right;
}
