//
// Created by matthew on 19.05.22.
//

#ifndef PROJECT_BIT_VECTOR_H
#define PROJECT_BIT_VECTOR_H

#define MAX_SIZE 400
#define MIN_SIZE 196

#include <vector>
#include <cstdint>
#include <cassert>

class Vertex;

class Vertexy {
public:
    // the height in the tree (used for balancing)
    int64_t height = 1;
    // the number of ones in the subtree (used for rank)
    int64_t num_ones = 0;
    // the number of bits stored in the subtree (used for finding), setting this to 0 invalidates a Vertexy object
    int64_t size = 0;

    Vertex *parent = nullptr;

    Vertexy() = default;

    virtual ~Vertexy() = default;

    virtual void flip(int64_t) = 0;

    virtual int64_t rank_0(int64_t) = 0;

    virtual int64_t rank_1(int64_t) = 0;

    virtual int64_t select_0(int64_t) = 0;

    virtual int64_t select_1(int64_t) = 0;

    virtual void remove(int64_t pos) = 0;

    /**
     * Find the rightmost child in the left subtree, and merge it with leftmost child of the right subtree
     *
     * @param pVertexy a vertex that lies on the shortex path between this and the next leaf
     */
    virtual void merge_right(Vertexy *pVertexy) = 0;

    virtual void add(int64_t to_add, int64_t pos) = 0;

    virtual uint64_t get_size() = 0;

    virtual bool is_Leaf() { return false; }
};

class Vertex : public Vertexy {
public:

    Vertexy *left;
    Vertexy *right;

    Vertex(const std::vector<uint16_t> &data, int64_t begin, int64_t end);

    void update();

    ~Vertex() override;

    void flip(int64_t pos) override;

    int64_t rank_0(int64_t pos) override;

    int64_t rank_1(int64_t pos) override;

    int64_t select_0(int64_t pos) override;

    int64_t select_1(int64_t pos) override;

    void remove(int64_t pos) override;

    void merge_right(Vertexy *pVertexy) override;

    void add(int64_t to_add, int64_t pos) override;

    void rotate_right();

    void rotate_left();

    void checkLeftKid();

    void checkRightKid();

    void rotate() {
        if (height - right->height > 2) {
            rotate_right();
        } else if (height - left->height > 2) {
            rotate_left();
        }
    }

    uint64_t get_size() override {
        return sizeof(*this) * 8 + left->get_size() + right->get_size();
    }
};

class Leaf : public Vertexy {
private:
    int64_t used_up = 0;

    /***
     * Updates num_ones in this leaf.
     */
    void update_num_ones() {
        num_ones = 0;
        for (int i = 0; i < used_up; ++i) {
            int64_t temp = data[i];
            for (int j = 0; j < 16; ++j) {
                num_ones += temp & 0b1;
                temp >>= 1;
            }
        }
    }

public:

    uint16_t data[MAX_SIZE + 1]{};

    Leaf() = default;

    /**
     * Construct a leaf given a partial vector. Leaf stores the data from [i,i1) of vector
     *
     * @param vector The vector storing the data
     * @param i the beginning index of the data
     * @param i1 the end index of the data
     */
    Leaf(const std::vector<uint16_t> &vector, int64_t i, int64_t i1);

    ~Leaf() override = default;

    void flip(int64_t pos) override;

    int64_t rank_0(int64_t pos) override;

    int64_t rank_1(int64_t pos) override;

    int64_t select_0(int64_t pos) override;

    int64_t select_1(int64_t pos) override;

    void merge_right(Vertexy *pVertexy) override;

    void remove(int64_t pos) override;

    void add(int64_t to_add, int64_t pos) override;

    bool is_Leaf() override { return true; }

    uint64_t get_size() override {
        return sizeof(*this) * 8;
    }
};

class BitVector {
private:
    Vertex *v;
public:
    explicit BitVector(const std::vector<uint16_t> &vec) {
        v = new Vertex(vec, 0, vec.size());
    }

    ~BitVector() {
        delete v;
    }

    /**
     * Rotations can change which root is the vertex. This method corrects for this.
     */
    void correct() {
        if (v->parent != nullptr) {
            v = v->parent;
            correct();
        }
    }

    void flip(int64_t pos) {
        uint64_t prev_num_ones = v->num_ones;
        uint64_t prev_size = v->size;
        v->flip(pos);
        assert(prev_num_ones == v->num_ones + 1 || prev_num_ones == v->num_ones - 1);
        assert(prev_size == v->size);
    };

    int64_t rank_0(int64_t pos) {
        return v->rank_0(pos);
    }

    int64_t rank_1(int64_t pos) {
        return v->rank_1(pos);
    }

    int64_t select_0(int64_t pos) {
        return v->select_0(pos);
    }

    int64_t select_1(int64_t pos) {
        return v->select_1(pos);
    }

    void remove(int64_t pos) {
        uint64_t prev_num_ones = v->num_ones;
        uint64_t prev_size = v->size;
        v->remove(pos);
        correct();
        assert(prev_num_ones == v->num_ones + 1 || prev_num_ones == v->num_ones);
        assert(prev_size == v->size + 1);
    }

    void add(int64_t to_add, int64_t pos) {
        uint64_t prev_num_ones = v->num_ones;
        uint64_t prev_size = v->size;
        v->add(to_add, pos);
        correct();
        assert(prev_size == v->size - 1);
        assert(prev_num_ones + to_add == v->num_ones);
    }

    uint64_t get_size() {
        return v->get_size();
    }
};


#endif //PROJECT_BIT_VECTOR_H
