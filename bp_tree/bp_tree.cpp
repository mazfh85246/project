//
// Created by matthew on 14.06.22.
//


#include <stdexcept>
#include <cassert>
#include "bp_tree.h"

// A lookup table for how much of a height decrease an int causes at its highest point
int8_t max_height_table[65536];
// a lookup table for the total height change caused by an int
int8_t height_change_table[65536];
// a lookup table for the number of ones in an int
int8_t num_ones_table[65536];

void initialise_lookup_tables() {
    for (int i = 0; i < 65536; ++i) {
        int8_t max = 0;
        int8_t total = 0;
        for (int j = 15; j >= 0; --j) {
            total += ((i >> j) & 1) ? 1 : -1;
            max = std::min(max, total);
            num_ones_table[i] += ((i >> j) & 1);
        }
        max_height_table[i] = max;
        height_change_table[i] = total;
    }
};


uint64_t TreeVertex::findClose(TreeVertexy *caller, int64_t height_remaining) {
    if (caller == parent) {
        // if the parent calls us
        if (left->max_height_change == 1) {
            update_params_rec();
        }
        if (left->max_height_change + height_remaining <= 0) {
            return left->findClose(this, height_remaining);
        } else {
            return left->size + right->findClose(this, height_remaining + left->get_height_change());
        }
    } else if (caller == left) {
        // if the left calls us
        if (right->max_height_change == 1) {
            right->update_params_rec();
        }
        if (right->max_height_change + height_remaining <= 0) {
            return right->findClose(this, height_remaining);
        } else {
            auto right_hight = right->get_height_change();
            return right->size + parent->findClose(this, height_remaining + right_hight);
        }
    } else {
        // if the right calls us
        return parent->findClose(this, height_remaining);
    }
}


int64_t TreeLeaf::rank_1(int64_t pos) {
    int64_t rank = 0;
    int64_t index = pos >> 4;
    int64_t number_of_bits = pos & 0b1111;
    int64_t bit_index = 16 - number_of_bits;
    for (int i = 0; i < index; ++i) {
        int64_t temp = this->data[i];
        for (int j = 0; j < 16; ++j) {
            rank += temp & 0x1;
            temp >>= 1;
        }
    }

    int64_t temp = this->data[index] >> (bit_index);
    for (int i = 0; i < number_of_bits; ++i) {
        rank += temp & 0x1;
        temp >>= 1;
    }
    return rank;
}

int64_t TreeLeaf::select_1(int64_t pos) {
    int64_t count = 0;
    for (int64_t temp: this->data) {
        for (int j = 15; j >= 0; --j) {
            pos -= temp >> j & 0b1;
            if (pos == 0) {
                return count;
            }
            count++;
        }
    }
    throw std::invalid_argument("ith index not found");
}

TreeLeaf::TreeLeaf(const std::vector<uint16_t> &data, int64_t begin, int64_t end) {
    int64_t si = end - begin;
    for (int64_t j = 0; j < si; ++j) {
        this->data[j] = data[j + begin];
    }
    this->used_up = end - begin;
    this->size = used_up * 16;

    update_params();
}

void TreeLeaf::remove(int64_t pos) {
    // find out by how many bits to the left we need to shift to get the relevant bit in the MSF position
    uint16_t bit_index = pos & 0b1111;
    uint64_t index = pos >> 4;

    // gets the value of the bit to be removed, so I can adjust parameters
    if ((data[index] >> (15 - bit_index)) & 0b1) {
        num_ones--;
    }

    uint16_t remove_lower = (16 - bit_index);
    data[index] = ((data[index] >> remove_lower) << (remove_lower - 1)) |
                  ((((data[index] << (bit_index + 1)) & 0xffff) >> bit_index) >> 1);

    for (uint64_t i = index; i < used_up; ++i) {
        // don't worry, our array is big enough so that this does not cause memory leaks!
        data[i] = (data[i] << 1) | (data[i + 1] >> 15);
    }
    size--;
    if ((size & 0b1111) == 0) {
        used_up--;
    }
    this->max_height_change = 1;
}

void TreeLeaf::merge_right(TreeVertexy *pVertexy) {
    if (!pVertexy->is_Leaf()) {
        merge_right(((TreeVertex *) pVertexy)->left);
        ((TreeVertex *) pVertexy)->update();
        ((TreeVertex *) pVertexy)->checkLeftKid();
        return;
    }
    auto *leaf = ((TreeLeaf *) pVertexy);
    // the shift required to put bits at the end of the int;
    int64_t right_shift = this->size & 0b1111;
    right_shift = (right_shift == 0) ? 16 : right_shift;
    // the shift required to put bits at the beginning of the int
    int64_t left_shift = 16 - right_shift;
    if (this->used_up + leaf->used_up <= BP_TREE_MAX_SIZE || parent->parent == nullptr) {
        //todo: this is correct, check if others are also correct
        // We can now simply merge the two, into one
        const int64_t till = leaf->used_up;
        for (int64_t i = 0; i < till; ++i) {
            this->data[used_up - 1 + i] |= leaf->data[i] >> right_shift;
            this->data[used_up + i] |= leaf->data[i] << left_shift;
        }
        this->size += leaf->size;
        this->used_up = this->size >> 4;
        this->used_up += (this->size % 16 == 0) ? 0 : 1;
        this->num_ones += leaf->num_ones;
        if (max_height_change == 1 || leaf->max_height_change == 1) {
            max_height_change = 1;
        } else {
            max_height_change = std::min(max_height_change, get_height_change() + leaf->max_height_change);
        }
        leaf->size = 0;
        leaf->height = 0;
        leaf->used_up = 0;
        leaf->max_height_change = 1;
        leaf->num_ones = 0;
    } else {
        // TODO: this code is never called. Why?
        // In this case, we transfer data from the smaller one to the larger one
        if (used_up > leaf->used_up) {
            const int64_t to_transfer = (used_up - leaf->used_up) / 2;
            // in this case, we need to transfer data from the left vertex, to the right vertex
            // first, move everything in the recipient over to the right
            for (int64_t i = leaf->used_up - 1; i != -1; --i) {
                // the first is an or, in order to maintain data placed by previous steps
                leaf->data[i + to_transfer] |= leaf->data[i] << left_shift;
                //the second is just an equal to remove the data in data[i+tt-1] before writing
                leaf->data[i + to_transfer - 1] = leaf->data[i] >> right_shift;
            }
            // now we have created space for the data to be moved into
            for (int64_t i = 0; i < to_transfer - 1; ++i) {
                leaf->data[i] = data[used_up - to_transfer + i];
                data[used_up - to_transfer + i] = 0;
            }
            leaf->data[to_transfer - 1] |= data[used_up - 1];
            data[used_up-1] = 0;
            // we have aligned the left leaf to 64 bits.
            int64_t old_size = size;
            size = (size % 16 == 0) ? ((size >> 4) - to_transfer) << 4 : ((size >> 4) + 1 - to_transfer) << 4;
            used_up = size >> 4;
            used_up += (size % 16 == 0) ? 0 : 1;
            leaf->size += old_size - size;
            leaf->used_up = leaf->size >> 4;
            leaf->used_up += leaf->size % 16 == 0 ? 0 : 1;
        } else {
            const int64_t to_transfer = (leaf->used_up - used_up) / 2;
            // in this case, we need to transfer data from the right to the left
            for (int i = 0; i < to_transfer; ++i) {
                data[used_up - 1 + i] |= leaf->data[i] >> right_shift;
                data[used_up + i] = leaf->data[i] << left_shift;
            }
            for (int64_t i = to_transfer; i < leaf->used_up; ++i) {
                leaf->data[i - to_transfer] = leaf->data[i];
                leaf->data[i] = 0;
            }
            leaf->used_up -= to_transfer;
            used_up += to_transfer;
            size += to_transfer * 16;
            leaf->size -= to_transfer * 16;
        }
        int64_t old_num_ones = num_ones;
        update_params();
        leaf->num_ones += old_num_ones - num_ones;
        leaf->max_height_change = 1;
        max_height_change = 1;
    }
}

void TreeLeaf::add(int64_t to_add, int64_t pos) {
    // here we can assume that we have enough space to add it, since splitting is handled by the parent.
    int64_t offset = pos >> 4;
    int64_t bitset = (pos & 0b1111);

    for (int64_t i = used_up; i > offset; --i) {
        data[i] >>= 1;
        data[i] |= data[i - 1] << 15;
    }
    // put the desired bit in the right position
    data[offset] = (data[offset] >> (16 - bitset) << (16 - bitset)) | ((to_add << (15 - bitset))) |
                   (((data[offset] << bitset) & 0xffff) >> (bitset + 1));

    if ((size & 0b1111) == 0) {
        used_up++;
    }
    size++;
    if (to_add == 1) {
        num_ones++;
    }
    max_height_change = 1;
}

uint64_t TreeLeaf::findClose(TreeVertexy *caller, int64_t height_remaining) {
    assert(max_height_change != 1);
    int i;
    for (i = 0; i < used_up; ++i) {
        if (height_remaining + max_height_table[data[i]] <= 0) {
            break;
        }
        height_remaining += height_change_table[data[i]];
    }
    // now we know where it is at, just gotta find the correct position
    for (int j = 15; j >= 0; --j) {
        height_remaining += ((data[i] >> j) & 1) ? 1 : -1;
        if (height_remaining == 0) {
            return (i << 4) | (15 - j);
        }
    }
    throw std::invalid_argument("closing bracket not found");
}

int64_t TreeLeaf::subtree_size(int64_t node_pos) {
    // here you have to calculate the height difference til the end of the array, and then call findClose!
    int64_t offset = node_pos >> 4;
    int64_t bit_offset = 15 - (node_pos & 0b1111);

    // first calculate the height difference till the end of this int
    int64_t height_diff = 0;
    int64_t iterate_til = (used_up - 1 == offset) ? 16 - (size & 0b1111) : 0;
    iterate_til = (used_up - 1 == offset && iterate_til == 16) ? 0 : iterate_til;
    for (int64_t i = bit_offset; i >= iterate_til; --i) {
        height_diff += (data[offset] >> i) & 0b1 ? 1 : -1;
        if (height_diff == 0) {
            return ((bit_offset - i) >> 1);
        }
    }

    int64_t found = -1;
    for (int64_t i = offset + 1; i < used_up - 1; ++i) {
        if (height_diff + max_height_table[data[i]] <= 0) {
            found = i;
            break;
        }
        height_diff += height_change_table[data[i]];
    }
    if (found == -1) {
        // check the last int, and if it is not there, call find close
        if (used_up - 1 != offset) {
            int64_t num_remaining = size & 0b1111;
            num_remaining = (num_remaining == 0) ? 0 : 16 - num_remaining;
            for (int j = 15; j >= num_remaining; --j) {
                height_diff += ((data[used_up - 1] >> j) & 1) ? 1 : -1;
                if (height_diff == 0) {
                    return (((used_up - 1) << 4) + (15 - j) - node_pos) >> 1;
                }
            }
        }
        // TODO: not sure about -1
        return ((size - node_pos - 1) + parent->findClose(this, height_diff)) >> 1;
    } else {
        // find it in the found position
        for (int j = 15; j >= 0; --j) {
            height_diff += ((data[found] >> j) & 1) ? 1 : -1;
            if (height_diff == 0) {
                return (((found << 4) + (15 - j)) - node_pos) >> 1;
            }
        }
    }

    return -1;
}

int64_t TreeVertex::select_1(int64_t pos) {
    if (pos <= left->num_ones) {
        return left->select_1(pos);
    }
    return left->size + right->select_1(pos - left->num_ones);
}


int64_t TreeVertex::rank_1(int64_t pos) {
    if (pos < left->size) {
        return left->rank_1(pos);
    }
    return left->num_ones + right->rank_1(pos - left->size);
}

void TreeVertex::remove(int64_t pos) {
    if (pos < left->size) {
        left->remove(pos);
    } else {
        right->remove(pos - left->size);
    }
    // todo: change to <= in bv
    if (left->size <= BP_TREE_MIN_SIZE << 4 || right->size <= BP_TREE_MIN_SIZE << 4) {
        left->merge_right(right);
        checkLeftKid();
        if (right != nullptr) {
            checkRightKid();
        }
    }
    if (left != nullptr && right != nullptr) {
        update();
        rotate();
    }
}

void TreeVertex::rotate_right() {
    auto *l = (TreeVertex *) left;
    left = l->right;
    left->parent = this;

    l->right = this;
    if (parent != nullptr) {
        if (parent->left == this) {
            parent->left = l;
        } else {
            parent->right = l;
        }
        l->parent = parent;
    } else {
        l->parent = nullptr;
    }
    parent = l;
    update();
    l->update();
}

void TreeVertex::rotate_left() {
    auto *r = (TreeVertex *) right;
    right = r->left;
    right->parent = this;

    r->left = this;
    if (parent != nullptr) {
        if (parent->left == this) {
            parent->left = r;
        } else {
            parent->right = r;
        }
        r->parent = parent;
    } else {
        r->parent = nullptr;
    }
    parent = r;
    update();
    r->update();
}

void TreeVertex::update() {
    height = std::max(left->height, right->height) + 1;
    num_ones = left->num_ones + right->num_ones;
    size = left->size + right->size;
    if (left->max_height_change == 1 || right->max_height_change == 1) {
        max_height_change = 1;
    } else {
        max_height_change = std::min(left->max_height_change, left->get_height_change() + right->max_height_change);
    }
}

void TreeVertex::merge_right(TreeVertexy *pVertexy) {
    right->merge_right(pVertexy);
    update();
    checkRightKid();
}

void TreeVertex::checkLeftKid() {
    if (left->size == 0 && parent != nullptr) {
        if (this->parent->left == this) {
            this->parent->left = right;
        } else {
            this->parent->right = right;
        }
        right->parent = parent;
        right = nullptr;
        delete this;
    } else {
        if (!left->is_Leaf()) {
            ((TreeVertex *) left)->checkLeftKid();
        }
    }
}

void TreeVertex::checkRightKid() {
    if (right->size == 0 && parent != nullptr) {
        if (this->parent->left == this) {
            this->parent->left = left;
        } else {
            this->parent->right = left;
        }
        left->parent = parent;
        left = nullptr;
        delete this;
    } else {
        if (!right->is_Leaf()) {
            ((TreeVertex *) right)->checkRightKid();
        }
    }
}

void TreeVertex::add(int64_t to_add, int64_t pos) {
    if (pos < left->size) {
        if (left->is_Leaf() && left->size >> 4 == BP_TREE_MAX_SIZE) {
            auto v = new TreeVertex(
                    std::vector<uint16_t>(((TreeLeaf *) left)->data, ((TreeLeaf *) left)->data + BP_TREE_MAX_SIZE), 0,
                    BP_TREE_MAX_SIZE);
            delete left;
            left = v;
            left->parent = this;
            update();
        }
        left->add(to_add, pos);
    } else {
        if (right->is_Leaf() && right->size >> 4 == BP_TREE_MAX_SIZE) {
            auto v = new TreeVertex(
                    std::vector<uint16_t>(((TreeLeaf *) right)->data, ((TreeLeaf *) right)->data + BP_TREE_MAX_SIZE), 0,
                    BP_TREE_MAX_SIZE);
            delete right;
            right = v;
            right->parent = this;
            update();
        }
        right->add(to_add, pos - left->size);
    }
    update();
    rotate();
}

TreeVertex::TreeVertex(const std::vector<uint16_t> &data, int64_t begin, int64_t end) {
    const int64_t num_els = end - begin;
    int64_t l_size = (num_els / 2) + 1;
    int64_t fend = begin + l_size;

    if (num_els > BP_TREE_MAX_SIZE) {
        left = new TreeVertex(data, begin, fend);
        left->parent = this;
        right = new TreeVertex(data, fend, end);
        right->parent = this;
    } else {
        left = new TreeLeaf(data, begin, fend);
        right = new TreeLeaf(data, fend, end);
    }
    left->parent = this;
    right->parent = this;
    update();
}

int64_t TreeVertex::subtree_size(int64_t pos) {
    if (pos < left->size) {
        return left->subtree_size(pos);
    }
    return right->subtree_size(pos - left->size);
}