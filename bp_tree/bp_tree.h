//
// Created by matthew on 14.06.22.
//
#include <iostream>
#include "../binary_vector/bit_vector.h"
#include <cassert>


#ifndef PROJECT_BP_TREE_H
#define PROJECT_BP_TREE_H

#define BP_TREE_MAX_SIZE 512
#define BP_TREE_MIN_SIZE 252

// A lookup table for how much of a height decrease an int causes at its highest point
extern int8_t max_height_table[65536];
// a lookup table for the total height change caused by an int
extern int8_t height_change_table[65536];
// a lookup table for the number of ones in an int
extern int8_t num_ones_table[65536];

extern void initialise_lookup_tables();

class TreeVertex;

class TreeVertexy {
public:
    // the height in the tree (used for balancing)
    int64_t height = 1;
    // the number of ones in the subtree (used for rank)
    int64_t num_ones = 0;

    uint64_t size = 0;

    int64_t max_height_change = 0;

    TreeVertexy() = default;

    virtual ~TreeVertexy() = default;;

    TreeVertex *parent = nullptr;

    virtual int64_t rank_1(int64_t) = 0;

    virtual int64_t select_1(int64_t) = 0;

    virtual void remove(int64_t pos) = 0;

    virtual void merge_right(TreeVertexy *pVertexy) = 0;

    virtual void add(int64_t to_add, int64_t pos) = 0;

    virtual uint64_t findClose(TreeVertexy *caller, int64_t height_remaining) = 0;

    virtual int64_t subtree_size(int64_t node_pos) = 0;

    virtual void update_params_rec() = 0;

    int64_t get_height_change() {
        return (num_ones << 1) - size;
    }

    virtual bool is_Leaf() { return false; }

    virtual uint64_t get_size() = 0;
};


class TreeVertex : public TreeVertexy {

public:

    TreeVertexy *left;
    TreeVertexy *right;

    void update();

    TreeVertex(const std::vector<uint16_t> &data, int64_t begin, int64_t end);

    ~TreeVertex() {
        delete left;
        delete right;
    }


    int64_t rank_1(int64_t pos) override;

    int64_t select_1(int64_t pos) override;

    void remove(int64_t pos) override;

    void merge_right(TreeVertexy *pVertexy) override;

    void add(int64_t to_add, int64_t pos) override;

    void rotate_right();

    void rotate_left();

    void checkLeftKid();

    void checkRightKid();

    void update_params_rec() override {
        if (left->max_height_change == 1) {
            left->update_params_rec();
        }
        if (right->max_height_change == 1) {
            right->update_params_rec();
        }
        update();
    }

    uint64_t findClose(TreeVertexy *caller, int64_t height_remaining) override;

    void rotate() {
        if (height - right->height > 2) {
            rotate_right();
        } else if (height - left->height > 2) {
            rotate_left();
        }
    }

    int64_t subtree_size(int64_t pos) override;

    uint64_t get_size() override {
        return sizeof(*this) * 8 + left->get_size() + right->get_size();
    }
};

class TreeLeaf : public TreeVertexy {
private:
    int64_t used_up = 0;

    void update_params() {
        num_ones = 0;
        int64_t max_diff = 0;
        int64_t h = 0;
        int iterate_till = ((this->size & 0b1111) == 0) ? used_up + 1 : used_up;
        for (int i = 0; i < iterate_till - 1; ++i) {
            num_ones += num_ones_table[data[i]];
            max_diff = std::min(h + max_height_table[data[i]], max_diff);
            h += height_change_table[data[i]];
        }
        // now we have to consider the last couple of bits
        int num_bits_remaining = size & 0b1111;

        for (int i = 15; i >= 16 - num_bits_remaining; --i) {
            h += ((data[used_up - 1] >> i) & 1) ? 1 : -1;
            max_diff = std::min(max_diff, h);
            num_ones += (data[used_up - 1] >> i) & 0b1;
        }

        max_height_change = max_diff;
    }

public:

    uint16_t data[BP_TREE_MAX_SIZE + 1]{};

    TreeLeaf() = default;

    TreeLeaf(const std::vector<uint16_t> &vector, int64_t i, int64_t i1);

    ~TreeLeaf() override = default;

    int64_t rank_1(int64_t pos) override;

    int64_t select_1(int64_t pos) override;

    void merge_right(TreeVertexy *pVertexy) override;

    void remove(int64_t pos) override;

    void add(int64_t to_add, int64_t pos) override;

    uint64_t findClose(TreeVertexy *caller, int64_t height_remaining) override;

    int64_t subtree_size(int64_t node_pos) override;

    void update_params_rec() override {
        auto old_num_ones = num_ones;
        update_params();
        assert(old_num_ones == num_ones);
    }

    bool is_Leaf() override { return true; }

    uint64_t get_size() override {
        return sizeof(*this);
    }
};

class BPTree {
    /***
     * This class labels its vertices starting with 1. The task requires using the labels starting with 0, so this has
     * to be accounted for
     */
public:
    TreeVertex *v;


    BPTree() {
        if (num_ones_table[1] == 0) {
            initialise_lookup_tables();
        }
        std::vector<uint16_t> vec = {0x8000};
        v = new TreeVertex(vec, 0, vec.size());
        for (int i = 0; i < 14; ++i) {
            v->remove(2);
        }
    }

    ~BPTree() {
        delete v;
    }


    void correct() {
        if (v->parent != nullptr) {
            v = v->parent;
            correct();
        }
        //assert(v->get_height_change() == 0);
    }

    void delete_node(int64_t node_num) {
        int64_t num_ones_before = v->num_ones;
        int64_t size_before = v->size;
        //node_num++;
        auto node_pos = v->select_1(node_num);
        auto close_pos = node_pos + (sub_tree_size_with_pos(node_pos) * 2) + 1;

        v->remove(close_pos);
        correct();

        v->remove(node_pos);
        correct();
        assert(num_ones_before == v->num_ones + 1);
        assert(size_before == v->size + 2);
        assert(v->num_ones == v->size / 2);
        assert(v->max_height_change == 0 || v->max_height_change == 1);
        assert(v->get_height_change() == 0);
        assert(v->parent == nullptr);
    }

    int64_t sub_tree_size(uint64_t node_num) {
        //node_num++;
        auto node_pos = v->select_1(node_num);
        return v->subtree_size(node_pos);
    }

    int64_t sub_tree_size_with_pos(int64_t node_pos) {
        return v->subtree_size(node_pos);
    }

    void insert_child(int64_t parent_num, int64_t child_index, int64_t num_children) {
        int64_t num_ones_before = v->num_ones;
        int64_t size_before = v->size;
        //parent_num++;
        child_index--;
        int64_t parent_pos = v->select_1(parent_num);
        int64_t child_insertion_pos = parent_pos + 1;
        for (int i = 0; i < child_index; ++i) {
            child_insertion_pos = child_insertion_pos + (sub_tree_size_with_pos(child_insertion_pos) * 2) + 2;
        }
        // now we know where the child open bracket will go, we need to find where the close bracket will go
        int64_t child_close_pos = child_insertion_pos;
        for (int i = 0; i < num_children; ++i) {
            child_close_pos = child_close_pos + (sub_tree_size_with_pos(child_close_pos) * 2) + 2;
        }
        v->add(0, child_close_pos);
        correct();
        v->add(1, child_insertion_pos);
        correct();
        assert(num_ones_before == v->num_ones - 1);
        assert(size_before == v->size - 2);
        assert(v->num_ones == v->size / 2);
        assert(v->max_height_change == 0 || v->max_height_change == 1);
        assert(v->get_height_change() == 0);
        assert(v->parent == nullptr);
    }

    int64_t parent(int64_t node_num) {
        //node_num++;
        auto node_pos = v->select_1(node_num);
        auto node_close = node_pos + (sub_tree_size_with_pos(node_pos) * 2) + 1;
        int64_t parent_pos = 0;
        int64_t current_child_pos = 0;
        // we will iterate through the children
        while (current_child_pos != node_pos) {
            // If the current child is above the vertex we are looking for
            if (current_child_pos + (sub_tree_size_with_pos(current_child_pos) * 2) + 1 >= node_close) {
                // go down one level
                parent_pos = current_child_pos;
                current_child_pos++;
            } else {
                current_child_pos = current_child_pos + sub_tree_size_with_pos(current_child_pos) * 2 + 2;
            }
        }
        return v->rank_1(parent_pos);
    }

    int64_t child(int64_t parent_num, int64_t child_index) {
        //parent_num++;
        child_index--;
        auto parent_pos = v->select_1(parent_num);
        int64_t child_pos = parent_pos + 1;
        for (int i = 0; i < child_index; ++i) {
            child_pos = child_pos + (sub_tree_size_with_pos(child_pos) * 2) + 2;
        }
        return v->rank_1(child_pos);
    }

    uint64_t get_size() {
        return v->get_size();
    }

    uint64_t get_degree(int64_t vertex_num) {
        auto pos = v->select_1(vertex_num);
        auto close_pos = pos + sub_tree_size_with_pos(pos) * 2 + 1;
        auto current_pos = pos + 1;
        uint64_t num_children = 0;
        while (current_pos != close_pos) {
            num_children++;
            current_pos = current_pos + sub_tree_size_with_pos(current_pos) * 2 + 2;
        }
        return num_children;
    }

    std::vector<uint64_t> get_degrees() {
        auto num_nodes = v->num_ones;
        std::vector<uint64_t> degrees;
        degrees.reserve(num_nodes);
        for (int i = 1; i < num_nodes; ++i) {
            degrees.push_back(get_degree(i));
        }
        return degrees;
    }
};


#endif //PROJECT_BP_TREE_H
