#include <iostream>
#include <fstream>
#include <bitset>
#include <chrono>
#include "binary_vector/bit_vector.h"
#include "bp_tree/bp_tree.h"

using namespace std;

int main(int argc, char **args) {
    if (argc != 4) {
        throw std::invalid_argument("wrong number of arguments");
    }

    string type = args[1];
    string in_file_name = args[2];
    ifstream in(in_file_name.c_str());
    string out_file_name = args[3];
    ofstream out(out_file_name.c_str());

    vector<uint64_t> output;
    uint64_t time = 0;
    uint64_t size = 0;

    if (type == "bv") {
        // first get the correct input
        string line;
        getline(in, line);
        std::vector<uint16_t> input;
        int n = stoi(line);
        int numints = n >> 4;
        input.reserve(numints);
        uint64_t next_input = 0;
        for (int i = 0; i < numints; ++i) {
            for (int j = 0; j < 16; ++j) {
                getline(in, line);
                next_input <<= 1;
                next_input |= stoi(line);
            }
            input.push_back(next_input);
        }
        int num_left = n & 0b1111;
        for (int j = 0; j < num_left; ++j) {
            getline(in, line);
            next_input <<= 1;
            next_input |= stoi(line);
        }
        if (num_left != 0) {
            input.push_back(next_input << (16 - num_left));
        }
        // finished calculating input at this point, moving on to the other thing

        // To do this, we implement a simple stack based command language for efficiency (like java!!)
        vector<int64_t> commands;

        while (!in.eof()) {
            // get the command
            getline(in, line, ' ');
            if (line == "insert") {
                commands.push_back(1);
                getline(in, line, ' ');
                commands.push_back(stoi(line));
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "delete") {
                commands.push_back(2);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "flip") {
                commands.push_back(3);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "rank") {
                getline(in, line, ' ');
                commands.push_back((line == "0") ? 4 : 5);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "select") {
                getline(in, line, ' ');
                commands.push_back((line == "0") ? 6 : 7);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            }
        }

        // now all commands are stored in commands, the size is stored in n
        uint64_t size_difference = input.size() * 16 - n;
        output.reserve(commands.size() / 2);

        // time starts now
        auto start = std::chrono::steady_clock::now();

        BitVector v(input);
        for (int i = 0; i < size_difference; ++i) {
            v.remove(n);
        }

        uint64_t i = 0;
        uint64_t commands_size = commands.size();
        while (i < commands_size) {
            switch (commands[i++]) {
                case 1:
                    // The parameters are flipped when compared to exercise
                    v.add(commands[i + 1], commands[i]);
                    i += 2;
                    break;
                case 2:
                    v.remove(commands[i]);
                    i++;
                    break;
                case 3:
                    v.flip(commands[i++]);
                    break;
                case 4:
                    output.push_back(v.rank_0(commands[i++]));
                    break;
                case 5:
                    output.push_back(v.rank_1(commands[i++]));
                    break;
                case 6:
                    output.push_back(v.select_0(commands[i++]));
                    break;
                case 7:
                    output.push_back(v.select_1(commands[i++]));
                    break;
                default:
                    throw invalid_argument("unknown command code");
            }
        }
        auto stop = std::chrono::steady_clock::now();

        time = chrono::duration_cast<chrono::milliseconds>(stop.time_since_epoch() - start.time_since_epoch()).count();
        size = v.get_size();
    } else if (type == "bp") {
        vector<int64_t> commands;
        string line;

        while (!in.eof()) {
            // get the command
            getline(in, line, ' ');
            if (line == "deletenode") {
                commands.push_back(1);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "insertchild") {
                commands.push_back(2);
                getline(in, line, ' ');
                commands.push_back(stoi(line));
                getline(in, line, ' ');
                commands.push_back(stoi(line));
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "child") {
                commands.push_back(3);
                getline(in, line, ' ');
                commands.push_back(stoi(line));
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "subtree_size") {
                commands.push_back(4);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            } else if (line == "parent") {
                commands.push_back(5);
                getline(in, line, '\n');
                commands.push_back(stoi(line));
            }
        }

        // now all commands are stored in commands, the size is stored in n
        output.reserve(commands.size() / 2);

        // time starts now
        auto start = std::chrono::steady_clock::now();

        BPTree tree;

        uint64_t i = 0;
        while (i < commands.size()) {
            switch (commands[i++]) {
                case 1:
                    tree.delete_node(commands[i++]);
                    break;
                case 2:
                    tree.insert_child(commands[i], commands[i + 1], commands[i + 2]);
                    i += 3;
                    break;
                case 3:
                    output.push_back(tree.child(commands[i], commands[i + 1]));
                    i += 2;
                    break;
                case 4:
                    output.push_back(tree.sub_tree_size(commands[i++]));
                    break;
                case 5:
                    output.push_back(tree.parent(commands[i++]));
                    break;
                default:
                    throw std::invalid_argument("unrecognized command code");
            }
        }

        auto stop = std::chrono::steady_clock::now();

        time = chrono::duration_cast<chrono::milliseconds>(stop.time_since_epoch() - start.time_since_epoch()).count();
        size = tree.get_size();
        auto degrees = tree.get_degrees();
        output.insert(output.end(), degrees.begin(), degrees.end());
    } else {
        throw std::invalid_argument("Unrecognized command " + type);
    }

    cout << "RESULT algo=<" << type << "> name=<Matthew Akram> time=<" << time << "> space=<" << size << '>' << endl;
    for (uint64_t i: output) {
        out << i << endl;
    }
    out.close();

    return 0;
}
